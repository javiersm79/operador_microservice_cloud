
package cl.methodo.operador.data.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;


@Embeddable
@Table(name = "operador_transaccion")
public class OperadorTransaccionIdEntity implements Serializable {
    
    @Column(name = "id_local")
    Integer idLocal;
    
    @Column(name = "id_transaccion")
    Integer idTransaccion;
    
    public Integer getIdLocal() {
        return this.idLocal;
    }

    public void getIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }
    
    public Integer getIdTransaccion() {
        return this.idLocal;
    }

    public void getIdTransaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }
}

