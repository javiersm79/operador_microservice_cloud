
package cl.methodo.operador.data.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "operador")
public class OperadorEntity implements Serializable {
    @Id
    @Column(unique = true)
    private Integer idOperador;
    
    @Column
    private String nombre;
    
    @Column
    private Integer idLocal;

    public Integer getIdOperador() {
        return idOperador;
    }

    public void setIdOperador(Integer id) {
        this.idOperador = id;
    }
    
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public Integer getIdLocal() {
        return this.idLocal;
    }

    public void getIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    
}
