
package cl.methodo.operador.data.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "tipo_registro")
public class TipoRegistroEntity implements Serializable {
    @Id
    @Column(unique = true)
    private Integer idTipoRegistro;
    
    @Column
    private String nombre;

    public Integer getIiTipoRegistro() {
        return idTipoRegistro;
    }

    public void setIdTipoRegistro(Integer idTipoRegistro) {
        this.idTipoRegistro = idTipoRegistro;
    }
    
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
}
