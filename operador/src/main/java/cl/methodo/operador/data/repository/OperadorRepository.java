/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.methodo.operador.data.repository;

import cl.methodo.operador.data.entity.OperadorEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rperez
 */
@Repository
public interface OperadorRepository extends JpaRepository<OperadorEntity, Integer> {

    OperadorEntity findByIdLocalAndIdOperador(Integer idlocal, Integer idoperador);
    
}
