/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.methodo.operador.data.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author rperez
 */
@Entity
@Table(name = "tb_usuario")
public class UsuarioEntity extends GenericEntity {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(unique = true)
    private String username;

    @Column
    private String password;
    
    @OneToMany
    private List<CadenaRestauranteEntity> cadenaRestaurantes;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<CadenaRestauranteEntity> getCadenaRestaurantes() {
        return cadenaRestaurantes;
    }

    public void setCadenaRestaurantes(List<CadenaRestauranteEntity> cadenaRestaurantes) {
        this.cadenaRestaurantes = cadenaRestaurantes;
    }

}
