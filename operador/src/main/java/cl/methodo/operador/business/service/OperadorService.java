
package cl.methodo.operador.business.service;

import cl.methodo.operador.business.util.Converter;
import cl.methodo.operador.business.util.MessageUtil;
import cl.methodo.operador.client.dto.PrivilegioDTO;
import cl.methodo.operador.client.dto.RptOperadorTransaccionDTO;
import cl.methodo.operador.client.exception.OperadorException;
import cl.methodo.operador.data.entity.OperadorEntity;
import cl.methodo.operador.data.repository.OperadorRepository;
import cl.methodo_commons.enums.Errors;
import cl.methodo_commons.enums.Estatus;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import static org.apache.tomcat.jni.User.username;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author rperez
 */
@Service
public class OperadorService {

    @Autowired
    private OperadorRepository operadorRepository;


    public RptOperadorTransaccionDTO findByLocalIdOperador(Integer idlocal,Integer idoperador) throws OperadorException {
        
        if (idoperador == null) {
            throw new OperadorException("idoperador is null");
        }
        
        if (idlocal == null) {
            throw new OperadorException("idlocal is null");
        }
        
        OperadorEntity operadorEntity = this.operadorRepository.findByIdLocalAndIdOperador(idlocal, idoperador);
        
        if (operadorEntity == null) {
            throw new OperadorException("ENTITY NOT FOUND");
        }
        
        RptOperadorTransaccionDTO rptOperadorTransaccionDTO = Converter.rptOperadorTransaccionConverter(operadorEntity);
        return rptOperadorTransaccionDTO;
    }

}
