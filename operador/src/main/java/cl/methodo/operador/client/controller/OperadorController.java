
package cl.methodo.operador.client.controller;

import cl.methodo.operador.business.service.OperadorService;
import cl.methodo.operador.business.service.UsuarioService;
import cl.methodo.operador.business.util.MessageUtil;
import cl.methodo.operador.client.dto.ApiResponseDTO;
import cl.methodo.operador.client.dto.RptOperadorTransaccionDTO;
import cl.methodo.operador.client.dto.UsuarioDTO;
import cl.methodo.operador.client.exception.OperadorException;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rperez
 */
@RestController
@RequestMapping(value = "/v1/operadores")
public class OperadorController {

    @Autowired
    private OperadorService operadorService;


    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "successful operation", response = ApiResponseDTO.class)        ,
        @ApiResponse(code = 404, message = "Data is not complete")})
    @RequestMapping(value = "/{operador}/locales/{local}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<ApiResponseDTO> getOperador(@PathVariable("local") Integer local, @PathVariable("operador") Integer operador) {
        try {
            RptOperadorTransaccionDTO rptOperadorTransaccionDTO = this.operadorService.findByLocalIdOperador(local, operador);
            ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
            apiResponseDTO.setMensaje("FOUND");
            apiResponseDTO.setData(rptOperadorTransaccionDTO);
            return new ResponseEntity<>(apiResponseDTO, HttpStatus.OK);
        } catch (OperadorException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
            apiResponseDTO.setMensaje(ex.getMessage());
            apiResponseDTO.setData(null);
            return new ResponseEntity<>(apiResponseDTO, HttpStatus.BAD_REQUEST);
        }
    }

//    @ApiResponses(value = {
//        @ApiResponse(code = 200, message = "successful operation", response = ApiResponseDTO.class)
//        ,
//        @ApiResponse(code = 404, message = "Data is not complete")})
//    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    ResponseEntity<ApiResponseDTO> getAll() {
//        try {
//            List<UsuarioDTO> usuarioDTOs = this.usuarioService.findAll();
//            ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
//            apiResponseDTO.setMensaje("FOUND");
//            apiResponseDTO.setData(usuarioDTOs);
//            return new ResponseEntity<>(apiResponseDTO, HttpStatus.OK);
//        } catch (OperadorException ex) {
//            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//        }
//    }
}
