package cl.methodo.operador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import static springfox.documentation.builders.PathSelectors.any;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
//@EnableEurekaClient
@EnableAutoConfiguration
@EnableSwagger2
public class OperadorApplication {

    public static void main(String[] args) {
        SpringApplication.run(OperadorApplication.class, args);
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("Operador").select()
                .apis(RequestHandlerSelectors.basePackage("cl.methodo.operador.client.controller"))
                .paths(any()).build().apiInfo(new ApiInfo("Servicios para gestionar operadores de Methodo", "", "1.0.0", null, new Contact("Arquitectura Methodo", null, "arquitectura@methodo.cl"), null, null));
    }
}
